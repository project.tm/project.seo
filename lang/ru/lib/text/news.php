<?php

$MESS['PROJECT_SEO_TEXT_TITLE'] = 'Адрес';
$MESS['PROJECT_SEO_TEXT_TEXT'] = 'Описание';
$MESS['PROJECT_SEO_TEXT_CATEGORY'] = 'Раздел';
$MESS['PROJECT_SEO_TEXT_DATE_CREATE'] = 'Дата создания';
$MESS['PROJECT_SEO_TEXT_CREATED_BY'] = 'Создал';
$MESS['PROJECT_SEO_TEXT_MODIFIED_BY'] = 'Изменил';