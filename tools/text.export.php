<?php

use Bitrix\Main\Loader,
    Project\Seo\Export\Text;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

if (Loader::includeModule('project.seo')) {
    Text::export('php://output');
    header('Content-Type: application/csv');
    header('Content-Disposition: attachment; filename=' . strtolower($_SERVER['SERVER_NAME']) . '.text.csv');
    header('Pragma: no-cache');
}