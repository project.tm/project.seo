<?php

namespace Project\Seo\Traits;

use Project\Tools\Utility\Content;
use SplFileObject,
    Project\Seo\View,
    Project\Seo\Utility;

trait Csv
{

    use Agent;

    static public function process($page)
    {
        $limit = static::LIMIT;
        $start = ($page - 1) * $limit;
        $end = ($page) * $limit;

        $pageIsNext = false;
        $filename = self::getFile();
        if (file_exists($filename)) {
            $key = -1;
            if (Content::isutf8(file_get_contents($filename))) {
                if (($handle = fopen($filename, "r")) !== FALSE) {
                    while (($data = fgetcsv($handle, 100000, ";")) !== FALSE) {
                        $key++;
                        if ($key < $start) {
                            continue;
                        }
                        if ($key >= $end) {
                            $pageIsNext = true;
                            continue;
                        }
                        if (empty($key) or empty($data)) {
                            continue;
                        }
                        static::importData($data);
                    };
                    fclose($handle);
                    View::processed($page, $limit, $key);
                }
            } else {
                Log::error('Файл не в utf-8');

            }
        }
        return $pageIsNext;
    }

}
