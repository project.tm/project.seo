<?php

namespace Project\Seo\Traits;

trait Event {

    static protected function getUrlParam() {
        global $APPLICATION; 
        return $APPLICATION->GetCurPageParam('', array("back_url_admin", "back_url", 'clear_cache', 'bitrix_include_areas', 'bxajaxid', 'set_filter', 'ajax'));
    }

    static protected function getUrl() {
        global $APPLICATION;
        $param = $_SERVER['REQUEST_SCHEME'] .'://'. $_SERVER['SERVER_NAME'] . self::getUrlParam();
        $page = $_SERVER['REQUEST_SCHEME'] .'://'. $_SERVER['SERVER_NAME'] . $APPLICATION->GetCurPage(false);
        if ($param !== $page) {
            return array(
                $param,
                $page
            );
        } else {
            return $page;
        }
    }

    static protected function getItem($arResult) {
        if ($arResult) {
            arsort($arResult);
            $arItem = array_shift($arResult);
            if ($arItem) {
                return $arItem;
            }
        }
        return false;
    }

}
