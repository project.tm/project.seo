<?php

namespace Project\Seo\Traits;

use cFile,
    Project\Seo\Config,
    Project\Seo\Data;

trait Agent {

    static public function getBackupDir($file) {
        return $_SERVER["DOCUMENT_ROOT"] . '/upload/' . Config::MODULE . '/backup/' . $file . '/';
    }

    static public function getBackupFile($file) {
        $path = self::getBackupDir($file) . date('Y.m.d') . '/' . date('H-i-s') . '.csv';
        CheckDirPath($path);
        return $path;
    }

    static public function clear() {
        pre(__FUNCTION__);
    }

    static public function getFile() {
        return $_SERVER["DOCUMENT_ROOT"] . cFile::GetPath(Data::get('FILE'));
    }

}
