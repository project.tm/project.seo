<?php

namespace Project\Seo;

class Data {

    public static function get($key) {
        return $_SESSION[__CLASS__][$key] ?: null;
    }

    public static function set($data) {
        $_SESSION[__CLASS__] = $data;
    }

    public static function clear() {
        unset($_SESSION[__CLASS__]);
    }

}
