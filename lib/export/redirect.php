<?php

namespace Project\Seo\Export;

use SplFileObject,
    Project\Seo\Utility,
    Project\Seo\Redirect\RedirectTable;

class Redirect {

    static public function export($file) {
        $file = new SplFileObject($file, 'w');
        $file->setFlags(SplFileObject::READ_CSV);
        $rsData = RedirectTable::getList(array(
                    'select' => array('URL', 'NEW_URL')
        ));
        $file->fputcsv(Utility::toWin1251(array(
                    'URL',
                    'NEW_URL',
                )), ';');
        while ($arItem = $rsData->Fetch()) {
            $file->fputcsv(Utility::toWin1251(array(
                        $arItem['URL'],
                        $arItem['NEW_URL'],
                    )), ';');
        }
    }

}
