<?php

namespace Project\Seo\Export;

use SplFileObject,
    Project\Seo\Utility,
    Project\Seo\Text\NewsTable;

class Text {

    static public function export($file) {
        $file = new SplFileObject($file, 'w');
        $file->setFlags(SplFileObject::READ_CSV);
        $rsData = NewsTable::getList(array(
                    'select' => array('CATEGORY.TITLE', 'CATEGORY.CODE', 'TITLE', 'TEXT')
        ));
        $file->fputcsv(array(
                    'SECTION',
                    'CODE',
                    'URL',
                    'TEXT',
                ), ';');
        while ($arItem = $rsData->Fetch()) {
            $file->fputcsv(array(
                        $arItem['PROJECT_SEO_TEXT_NEWS_CATEGORY_TITLE'],
                        $arItem['PROJECT_SEO_TEXT_NEWS_CATEGORY_CODE'],
                        $arItem['TITLE'],
                        $arItem['TEXT'],
                    ), ';');
        }
    }

}
