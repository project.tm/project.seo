<?php

namespace Project\Seo\Text\AdminInterface;

use DigitalWand\AdminHelper\Helper\AdminSectionListHelper;

class CategoriesListHelper extends AdminSectionListHelper
{
	protected static $model = '\Project\Seo\Text\CategoriesTable';
}