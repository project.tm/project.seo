<?php

namespace Project\Seo\Text\AdminInterface;

use Bitrix\Main\Localization\Loc;
use DigitalWand\AdminHelper\Helper\AdminSectionEditHelper;

Loc::loadMessages(__FILE__);

class CategoriesEditHelper extends AdminSectionEditHelper
{
    protected static $model = '\Project\Seo\Text\CategoriesTable';

    /**
     * @inheritdoc
     */
    public function setTitle($title)
    {
        if (!empty($this->data)) {
            $title = Loc::getMessage('PROJECT_SEO_TEXT_CATEGORY_EDIT_TITLE', array('#ID#' => $this->data[$this->pk()]));
        }
        else {
            $title = Loc::getMessage('PROJECT_SEO_TEXT_CATEGORY_NEW_TITLE');
        }

        parent::setTitle($title);
    }
}