<?php

namespace Project\Seo\Import;

use Bitrix\Main\Entity\DataManager,
    Bitrix\Main;

/**
 * Модель новостей.
 */
class ImportTable extends DataManager {

    /**
     * {@inheritdoc}
     */
    public static function getTableName() {
        return 'd_project_seo_import';
    }

    /**
     * {@inheritdoc}
     */
    public static function getMap() {
        return array(
            new Main\Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
                    )),
            new Main\Entity\IntegerField('CLEAR', array(
                'title' => 'Очистить данные'
                    )),
            new Main\Entity\IntegerField('FILE', array(
                'title' => 'Выберите файл'
                    )),
            new Main\Entity\StringField('FILE_TYPE', array(
                'title' => 'Тип загрузки',
                    ))
        );
    }

    public static function add(array $data) {

    }

}
