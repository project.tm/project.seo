<?

use Project\Seo\Config,
    Project\Seo\Backup\AdminInterface\BackupListHelper,
    Project\Seo\Parse\Meta;
?>
<h3>Экспорт:</h3>
<p><a href="/local/modules/<?= Config::MODULE ?>/tools/redirect.export.php">Экспорт редиректов</a></p>
<p><a href="/local/modules/<?= Config::MODULE ?>/tools/meta.export.php">Экспорт мета-тегов</a></p>
<p><a href="/local/modules/<?= Config::MODULE ?>/tools/text.export.php">Экспорт текстов</a></p>
<div class="adm-info-message">
    <p>Для текста, необходимо разместить в коде код: &lt;!-- #CODE# --&gt;</p>
</div>
<br>
<!--<div class="adm-info-message">
    <p>Перед импортом, делается бекап данных, в свою папку, с разбитием по дате, и времени</p>
    <p><?= Meta::getBackupDir('redirect') ?></p>
    <p><?= Meta::getBackupDir('meta') ?></p>
    <p><?= Meta::getBackupDir('text') ?></p>
</div>-->
<div class="adm-info-message">
    <p>Перед импортом, делается <a href="<?= BackupListHelper::getUrl() ?>">бекап данных</a></p>
</div>
<h3>Примеры:</h3>
<p><a href="/local/modules/<?= Config::MODULE ?>/example/example.redirect.csv">Файл редиректов</a></p>
<p><a href="/local/modules/<?= Config::MODULE ?>/example/example.meta.csv">Файл мета-тегов</a></p>
<p><a href="/local/modules/<?= Config::MODULE ?>/example/example.text.csv">Файл текстов</a></p>