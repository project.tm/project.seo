<?php

namespace Project\Seo\Parse;

use Project\Seo\Redirect\RedirectTable,
    Project\Seo\Helper\Backup,
    Project\Seo\Export,
    Project\Seo\Log,
    Project\Seo\Traits;

class Redirect {

    const LIMIT = 100;

    use Traits\Csv;

    static public function backup() {
        Export\Redirect::export($file = self::getBackupFile('redirect'));
        Backup::saveLog('Redirect', $file);
    }

    static public function clear() {
        RedirectTable::truncate();
    }

    static public function importData($arData) {
        if (count($arData) != 2) {
            Log::success('Редиректы: Не известная строка', var_export($arData, true));
            return;
        }
        $arData = array(
            'URL' => $arData[0],
            'NEW_URL' => $arData[1],
        );
        if (empty($arData['URL']) or empty($arData['NEW_URL'])) {
            Log::success('Редиректы: Пустой адрес', var_export($arData, true));
            return;
        }
        $rsData = RedirectTable::getList(array(
                    "select" => array('ID'),
                    'filter' => array('URL' => $arData['URL'])
        ));
        if ($arItem = $rsData->Fetch()) {
            unset($arItem['URL']);
            RedirectTable::update($arItem['ID'], $arData);
        } else {
            RedirectTable::add($arData);
        }
        Log::success('Редиректы: Обновлены страницы', $arData['URL']);
    }

}
