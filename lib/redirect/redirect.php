<?php

namespace Project\Seo\Redirect;

use Bitrix\Main\Entity\DataManager,
    Bitrix\Main,
    Project\Seo\Config;

class RedirectTable extends DataManager {

    public static function truncate() {
        static::getEntity()->getConnection()->query("TRUNCATE " . self::getTableName() . ";");
    }

    public static function getTableName() {
        return 'd_project_seo_redirect';
    }

    public static function getMap() {
        return array(
            new Main\Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
                    )),
            new Main\Entity\StringField('URL', array(
                'title' => 'Адрес'
                    )),
            new Main\Entity\StringField('NEW_URL', array(
                'title' => 'Новый адрес'
                    ))
        );
    }

}
