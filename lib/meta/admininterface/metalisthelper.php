<?php

namespace Project\Seo\Meta\AdminInterface;

use DigitalWand\AdminHelper\Helper\AdminListHelper;

/**
 * Хелпер описывает интерфейс, выводящий список новостей.
 *
 * {@inheritdoc}
 */
class MetaListHelper extends AdminListHelper {

    protected static $model = 'Project\Seo\Meta\MetaTable';

}
