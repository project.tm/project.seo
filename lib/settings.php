<?php

namespace Project\Seo;

use cOption;

class Settings {

    static public function get($key) {
        if ((int) SM_VERSION < 16) {
            return cOption::GetOptionString(Config::MODULE, $key);
        } else {
            return \Bitrix\Main\Config\Option::get(Config::MODULE, $key);
        }
    }

}
