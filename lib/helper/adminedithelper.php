<?php

namespace Project\Seo\Helper;

use DigitalWand\AdminHelper\Helper;

/**
 * Хелпер описывает интерфейс, выводящий форму редактирования новости.
 *
 * {@inheritdoc}
 */
abstract class AdminEditHelper extends Helper\AdminEditHelper {

    protected function getMenu($showDeleteButton = true) {
        return array();
    }

    protected function saveElement($id = null) {
        return array();
    }

    protected function show404() {

    }

    public function show() {
        if ((int) SM_VERSION < 16) {
            ?><div class="adm-workarea adm-workarea-page"><?
            }
            parent::show();
            if ((int) SM_VERSION < 16) {
                ?></div><?
        }
    }

}
