<?php

namespace Project\Seo;

class Config {

    const MODULE = 'project.seo';
    const IS_CACHE = true;
    const LIMIT = 30;
    public static $ROUTER = array(
        'Meta' => 'Импорт мета тегов',
        'Redirect' => 'Импорт редиректов',
        'Text' => 'Импорт текстов',
    );

}
