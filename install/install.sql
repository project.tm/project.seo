DROP TABLE IF EXISTS d_project_seo_import;
CREATE TABLE IF NOT EXISTS d_project_seo_import (
    `ID` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `CLEAR` INT,
    `FILE` INT,
    `FILE_TYPE` VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS d_project_seo_import_backup (
    `ID` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `TYPE` VARCHAR(255),
    `DATE` datetime,
    `FILE` int(18)
);

CREATE TABLE IF NOT EXISTS d_project_seo_meta (
    `ID` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `URL` VARCHAR(255),
    `H1` text,
    `TITLE` text,
    `KEYWORDS` text,
    `DESCRIPTION` text,
    `META` text
);

CREATE TABLE IF NOT EXISTS d_project_seo_redirect (
    `ID` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `URL` VARCHAR(255),
    `NEW_URL` VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS `d_project_seo_text` (
    `ID`             INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `CATEGORY_ID`    INT,
    `DATE_CREATE`    DATETIME,
    `CREATED_BY`     INT,
    `MODIFIED_BY`    INT,
    `TITLE`          VARCHAR(255),
    `TEXT`           LONGTEXT,
    `TEXT_TEXT_TYPE` VARCHAR(255),
    `SOURCE`         VARCHAR(255),
    `IMAGE`          INT
);
CREATE TABLE IF NOT EXISTS d_project_seo_text_categories (
    `ID`          INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `PARENT_ID`   INT,
    `DATE_CREATE` DATETIME,
    `CREATED_BY`  INT,
    `MODIFIED_BY` INT,
    `TITLE`       VARCHAR(255),
    `CODE`       VARCHAR(255)
);